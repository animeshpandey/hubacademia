// project group service client
(function() {
    angular
        .module("HubAcademiaApp")
        .factory("GroupService", GroupService);

    function GroupService($http) {
        function leaveGroup(userId, groupId) {
            var url = "/api/user/" + userId + "/group/" + groupId;
            return $http.delete(url);
        }

        function updateGroup(newGroup) {
            var url = "/api/group/" + newGroup._id;
            return $http.put(url, newGroup);
        }

        function findGroupByGroupId(groupId) {
            var url = "/api/group?groupId=" + groupId;
            return $http.get(url);
        }

        function dismissGroup(groupId) {
            var url = "/api/group/" + groupId;
            return $http.delete(url);
        }

        function createGroup(groupName, leaderId, members, bookId, bookTitle, bookSmallThumbnail) {
            var groupInfo = {
                groupName: groupName,
                leaderId:leaderId,
                members: members,
                bookId: bookId,
                bookTitle: bookTitle,
                bookSmallThumbnail: bookSmallThumbnail
            };

            var url = "/api/newgroup";
            return $http.post(url, groupInfo);
        }

        function findGroupsIamIn(memberId) {
            var url = "/api/group?memberId=" + memberId;
            return $http.get(url);
        }


        function findMyOwnedGroup(userId, bookId) {
            var url = "/api/user/" + userId + "/literature/" + bookId + "/group";
            return $http.get(url);
        }
        
        function findMyOwnedGroupsByUserId(leaderId) {
            var url = "/api/group?leaderId=" + leaderId;
            return $http.get(url);
        }

        var api = {
            findMyOwnedGroup : findMyOwnedGroup,
            createGroup: createGroup,
            findMyOwnedGroupsByUserId: findMyOwnedGroupsByUserId,
            findGroupsIamIn: findGroupsIamIn,
            dismissGroup: dismissGroup,
            findGroupByGroupId: findGroupByGroupId,
            updateGroup: updateGroup,
            leaveGroup: leaveGroup
        };
        return api;
    }
})();


(function(){
    angular
        .module("HubAcademiaApp")
        .factory("GoogleBooksService", GoogleBooksService);

    var key = "AIzaSyCLqFed4z7hEGEkCo1KqzDVZv0t4fREpVc";
    var quickSearchUrlBase = "https://www.googleapis.com/books/v1/volumes?q=TEXT&maxResults=MAX_RESULTS&key=API_KEY&country=US";
    var completeUrlBase = "https://www.googleapis.com/books/v1/volumes?q=TEXT+intitle:TITLE+inauthor:AUTHOR+inpublisher:PUBLISHER+isbn:ISBN+subject:SUBJECT&maxResults=MAX_NUM&key=API_KEY&country=US";
    var notAvailable = "not available";
    var authorNumLimit = 2;
    var titleLengthLimit = 100;
    var quickSearchMaxResults = 8;

    function GoogleBooksService($http) {
        function extractInfo(googleBookReturn) {
            var result = [];
            if (googleBookReturn) {
                var books = googleBookReturn.items;
                var totalItems = googleBookReturn.totalItems;
                var tmpBook = {};
                var tmpInfo = {};
                for (var i in books) {
                    tmpBook = books[i].volumeInfo;
                    tmpInfo = {
                        bookId: books[i].id,
                        title: "",
                        subtitle: "",
                        publisher: "",
                        publishedDate: "",
                        description: "",
                        authors: [],
                        pageCount: 0,
                        identifier: "",
                        identifierType: "",
                        previewLink: "",
                        smallThumbnail: "/img/img-not-found.png"
                    };
                    if (tmpBook.hasOwnProperty('title')) {
                        tmpInfo.title = tmpBook.title;
                    }
                    if (tmpBook.hasOwnProperty('subtitle')) {
                        tmpInfo.subtitle = tmpBook.subtitle;
                    }
                    if (tmpBook.hasOwnProperty('publisher')) {
                        tmpInfo.publisher = tmpBook.publisher;
                    }
                    if (tmpBook.hasOwnProperty('publishedDate')) {
                        tmpInfo.publishedDate = tmpBook.publishedDate;
                    }
                    if (tmpBook.hasOwnProperty('description')) {
                        tmpInfo.description = tmpBook.description;
                    }
                    if (tmpBook.hasOwnProperty('authors')) {
                        tmpInfo.authors = tmpBook.authors;
                    }
                    if (tmpBook.hasOwnProperty('pageCount')) {
                        tmpInfo.pageCount = tmpBook.pageCount;
                    }
                    if (tmpBook.hasOwnProperty('industryIdentifiers')) {
                        tmpInfo.identifier = tmpBook.industryIdentifiers[0].identifier;
                        tmpInfo.identifierType = tmpBook.industryIdentifiers[0].type;
                    }
                    if (tmpBook.hasOwnProperty('previewLink')) {
                        tmpInfo.previewLink = tmpBook.previewLink;
                    }
                    if (tmpBook.hasOwnProperty('imageLinks')) {
                        tmpInfo.smallThumbnail = tmpBook.imageLinks.smallThumbnail;
                    }
                    result.push(tmpInfo);
                }
            }
            return {
                totalItems: totalItems,
                samples: result
            };
        }

        function getTitle(book) {
            var title = book.title;
            var subtitle = book.subtitle;
            var completeTitle = "";
            if (title) {
                if (subtitle) {
                    completeTitle = title + ": " + subtitle;
                    if (completeTitle.length < titleLengthLimit) {
                        title = title + ": " + subtitle;
                    } else if (title.length < titleLengthLimit) {
                        title = title;
                    } else if (subtitle.length < titleLengthLimit) {
                        title = subtitle;
                    } else {
                        title = notAvailable;
                    }
                } else if (title.length >= titleLengthLimit) {
                    title = title;
                } else {
                    title = notAvailable;
                }
            } else if (subtitle) {
                if (subtitle.length < titleLengthLimit) {
                    title = subtitle;
                } else {
                    title = notAvailable;
                }
            } else {
                title = notAvailable;
            }
            return title;
        }

        function getAuthors(book) {
            var authors = book.authors;
            var authorString = "";
            var len = authors.length;

            for (var i = 0; i < Math.min(authorNumLimit, len); i++) {
                authorString += authors[i] + ", ";
            }
            authorString = authorString.substring(0, authorString.length-2);
            return authorString;
        }
        
        function quickSearch(searchText) {
            var url = quickSearchUrlBase
                .replace("MAX_RESULTS", ""+quickSearchMaxResults)
                .replace("API_KEY", key)
                .replace("TEXT", searchText);
            return $http.get(url);
        }

        function advancedSearch(searchInfo) {
            var url = completeUrlBase
                .replace("API_KEY", key)
                .replace("TEXT", searchInfo.keyWord);
            if (searchInfo.hasOwnProperty('bookTitle') && searchInfo.bookTitle) {
                url = url.replace("TITLE", searchInfo.bookTitle);
            } else {
                url = url.replace("+intitle:TITLE", "");
            }
            if (searchInfo.hasOwnProperty('author') && searchInfo.author) {
                url = url.replace("AUTHOR", searchInfo.author);
            } else {
                url = url.replace("+inauthor:AUTHOR", "");
            }
            if (searchInfo.hasOwnProperty('publisher') && searchInfo.publisher) {
                url = url.replace("PUBLISHER", searchInfo.publisher);
            } else {
                url = url.replace("+inpublisher:PUBLISHER", "");
            }
            if (searchInfo.hasOwnProperty('ISBN') && searchInfo.ISBN) {
                url = url.replace("ISBN", searchInfo.ISBN);
            } else {
                url = url.replace("+isbn:ISBN", "");
            }
            if (searchInfo.hasOwnProperty('subject') && searchInfo.subject) {
                url = url.replace("SUBJECT", searchInfo.subject);
            } else {
                url = url.replace("+subject:SUBJECT", "");
            }
            if (searchInfo.hasOwnProperty('maxNum') && searchInfo.maxNum) {
                url = url.replace("MAX_NUM", ""+(searchInfo.maxNum));
            } else {
                url = url.replace("MAX_NUM", ""+40);
            }
            return $http.get(url);
        }

        var api = {
            quickSearch: quickSearch,
            advancedSearch: advancedSearch,
            extractInfo: extractInfo,
            getTitle: getTitle,
            getAuthors: getAuthors
        };
        return api;
    }
})();

(function() {
    angular
        .module("HubAcademiaApp")
        .factory("MyBookService", MyBookService);

    function MyBookService($http) {
        function deleteReview(userId, bookId) {
            var url = "/api/bookshelf_user/" + userId + "/literature/" + bookId;
            return $http.put(url, {review: ""});
        }

        function updateMyBook(userId, bookId, newMyBook) {
            var url = "/api/bookshelf_user/" + userId + "/literature/" + bookId;
            return $http.put(url, newMyBook);
        }

        function findBookByBookIdWithUserId(userId, bookId) {
            var url = "/api/bookshelf_user/" + userId + "/literature/" + bookId;
            return $http.get(url);
        }
        
        function addBookFromDBtoMyBook(userId, bookId) {
            var newBook = {
                ownerId: userId,
                bookId: bookId,
                review: "",
                rating: "A good read",
                private: true
            };
            var url = "/api/bookshelf_user/" + userId + "/literature";
            return $http.post(url, newBook);
        }

        // given the user ID, return the user's favorite books through promise
        function findBooksByUserId(userId) {
            var url = "/api/bookshelf_user/"+ userId + "/literature";
            return $http.get(url);
        }
        
        // given literature ID, remove the literature from favorite literature list
        function removeBookById(userId, bookId) {
            var url = "/api/bookshelf_user/" + userId + "/literature/" + bookId;
            return $http.delete(url);
        }

        var api = {
            findBooksByUserId : findBooksByUserId,
            removeBookById : removeBookById,
            addBookFromDBtoMyBook : addBookFromDBtoMyBook,
            findBookByBookIdWithUserId : findBookByBookIdWithUserId,
            updateMyBook : updateMyBook,
            deleteReview: deleteReview
        };
        return api;
    }
})();

(function() {
    angular
        .module("HubAcademiaApp")
        .factory("ReviewService", ReviewService);

    function ReviewService($http) {
        function updateReview(reviewId, newReview) {
            var url = "/api/review/" + reviewId;
            return $http.put(url, newReview);
        }

        function findReviewById(reviewId) {
            var url = "/api/review/" + reviewId;
            return $http.get(url);
        }

        function deleteReview(reviewId) {
            var url = "/api/review/" + reviewId;
            return $http.delete(url);
        }

        function deleteFollowUp(reviewIdFollowed, followUpId) {
            var url = "/api/review/" + reviewIdFollowed + "/followup/" + followUpId;
            return $http.delete(url);
        }

        function followUp(reviewIdFollowed, username, comment) {
            var newFollowUp = {
                reviewIdFollowed: reviewIdFollowed,
                username: username,
                comment: comment
            };
            var url = "/api/review/" + reviewIdFollowed + "/followup";
            return $http.post(url, newFollowUp);
        }

        function findReviewsByGroupId(groupId) {
            var url = "/api/group/" + groupId + "/review";
            return $http.get(url);
        }

        function createNewReview(reviewerId, reviewerName, bookId, groupId, review, rating) {
            var newReview = {
                reviewerId: reviewerId,
                reviewerName: reviewerName,
                bookId: bookId,
                groupId: groupId,
                review: review,
                rating: rating,
                followUps: []
            };
            var url = "/api/review";
            return $http.post(url, newReview);
        }

        var api = {
            createNewReview: createNewReview,
            findReviewsByGroupId: findReviewsByGroupId,
            findReviewById: findReviewById,
            updateReview: updateReview,
            followUp: followUp,
            deleteFollowUp: deleteFollowUp,
            deleteReview: deleteReview
        };
        return api;
    }
})();


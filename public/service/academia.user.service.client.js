// project user service client
(function() {
    angular
        .module("HubAcademiaApp")
        .factory("UserService", UserService);

    function UserService($http) {
        function register(username, password) {
            var newUser = {
                username: username,
                password: password,
                firstName: "",
                lastName: ""
            };

            return $http.post("/api/bookshelf_user_register", newUser);
        }
        
        function loggedIn() {
            var url = "/api/bookshelf_user_loggedIn";
            return $http.get(url);
        }

        function logout() {
            var url = "/api/bookshelf_user_logout";
            return $http.post(url);
        }
        
        function login(username, password) {
            var user = {
                username: username,
                password: password
            };

            return $http.post("/api/bookshelf_user_login", user);
        }

        function findUserByUsername(username) {
            var url = "/api/bookshelf_user?username=" + username;
            return $http.get(url);
        }
        
        // add new user
        function createUser(username, password) {
            var newUser = {
                username: username,
                password: password,
                firstName: "",
                lastName: ""
            };

            return $http.post("/api/bookshelf_user", newUser);
        }

        // returns the user in local users array whose _id matches the userId parameter
        function findUserById(id) {
            var url = "/api/bookshelf_user/" + id;
            return $http.get(url);

        }

        // returns the user whose username and password match the username and password parameters
        function findUserByCredentials(username, password) {
            var url = "/api/bookshelf_user?username="+username+"&password="+password;
            return $http.get(url);
        }

        // updates the user in local users array whose _id matches the userId parameter
        function updateUser(userId, newUser) {
            var url = "/api/bookshelf_user/" + userId;
            return $http.put(url, newUser);
        }

        var api = {
            createUser : createUser,
            register: register,
            login: login,
            loggedIn: loggedIn,
            logout: logout,
            findUserById : findUserById,
            findUserByUsername : findUserByUsername,
            findUserByCredentials : findUserByCredentials,
            updateUser : updateUser
        };

        return api;
    }
})();


(function() {
    angular
        .module("HubAcademiaApp")
        .factory("ProgressService", ProgressService);

    function ProgressService($http) {
        function findProgressByUserIdBookId(userId, bookId) {
            var url = "/api/bookshelf_user/" + userId + "/literature/" + bookId + "/progress";
            return $http.get(url);
        }

        function removeRecord(progressId, recordId) {
            var url = "/api/progress/" + progressId + "/record/" + recordId;
            return $http.delete(url);
        }
        
        function addRecord(date, chapter, page, remark, progressId) {
            var newRecord = {
                date: date,
                chapter: chapter,
                page: page,
                remark: remark
            };
            
            var url = "/api/progress/" + progressId;
            return $http.put(url, newRecord);
        }

        function createProgress(userId, bookId) {
            var newProgress = {
                ownerId: userId,
                bookId: bookId,
                progress: [],
                currentPage: 0
            };
            var url = "/api/progress";
            return $http.post(url, newProgress);
        }

        var api = {
            createProgress: createProgress,
            findProgressByUserIdBookId: findProgressByUserIdBookId,
            addRecord: addRecord,
            removeRecord: removeRecord
        };
        return api;
    }
})();


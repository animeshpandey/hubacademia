(function() {
    angular
        .module("HubAcademiaApp")
        .factory("BookDetailService", BookDetailService);

    function BookDetailService($http) {
        var notAvailable = "not available";

        function createBookDetail(book) {
            var url = "/api/book_detail";
            return $http.post(url, book);
        }

        // given the literature ID, return the detail information of the literature
        function findBookDetailByBookId(bookId) {
            var url = "/api/book_detail/" + bookId;
            return $http.get(url);
        }

        function getFullTitle(title, subtitle) {
            if (title) {
                if (subtitle) {
                    title = title + ": " + subtitle;
                }
            } else if (subtitle) {
                title = subtitle;
            } else {
                title = notAvailable;
            }
            return title;
        }

        function getIndustrialIdentifier(identifier, type) {
            var indIdentifier = "";
            if (type) {
                if (type !== "other" && type !== "OTHER") {
                    indIdentifier = type + "-" + identifier;
                } else {
                    indIdentifier = identifier;
                }
            } else {
                if (identifier) {
                    indIdentifier = identifier;
                } else {
                    indIdentifier = notAvailable;
                }
            }
            return indIdentifier;
        }

        function getFullAuthors(authors, lengthLimit) {
            var authorString = "";
            for (var i = 0; i < lengthLimit; i++) {
                authorString += authors[i] + ", ";
            }
            authorString = authorString.substring(0, authorString.length-2);
            return authorString;
        }

        var api = {
            findBookDetailByBookId : findBookDetailByBookId,
            createBookDetail: createBookDetail,
            getFullTitle: getFullTitle,
            getIndustrialIdentifier: getIndustrialIdentifier,
            getFullAuthors: getFullAuthors
        };
        return api;
    }
})();


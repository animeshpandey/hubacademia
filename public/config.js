(function() {
    angular
        .module("HubAcademiaApp")
        .config(Config);

    function Config($routeProvider) {
        $routeProvider
            .when("/home", {
                templateUrl: "views/home/home.html",
                controller: "HomeController",
                controllerAs: "model"
            })
            .when("default", {
                templateUrl: "views/home/home.html",
                controller: "HomeController",
                controllerAs: "model"
            })
            .when("/login", {
                templateUrl: "views/user/academia.login.view.client.html",
                controller: "LoginController",
                controllerAs: "model"
            })
            .when("/register", {
                templateUrl: "views/user/academia.register.view.client.html",
                controller: "RegisterController",
                controllerAs: "model"
            })
            .when("/user", {
                templateUrl: "views/user/academia.profile.view.client.html",
                controller: "ProfileController",
                controllerAs: "model",
                resolve: {
                    loggedIn: checkLoggedIn
                }
            })
            .when("/user/:userId", {
                templateUrl: "views/user/academia.profile.view.client.html",
                controller: "ProfileController",
                controllerAs: "model",
                resolve: {
                    loggedIn: checkLoggedIn
                }
            })
            .when("/user/:userId/search", {
                templateUrl: "views/literature/academia.search_literature.view.client.html",
                controller: "BookSearchController",
                controllerAs: "model",
                resolve: {
                    loggedIn: checkLoggedIn
                }
            })
            .when("/user/:userId/literature", {
                templateUrl: "views/literature/academia.my_literature.view.client.html",
                controller: "MyBookController",
                controllerAs: "model",
                resolve: {
                    loggedIn: checkLoggedIn
                }
            })
            .when("/user/:userId/literature/:bookId", {
                templateUrl: "views/literature/academia.literature_detail.view.client.html",
                controller: "BookDetailController",
                controllerAs: "model",
                resolve: {
                    loggedIn: checkLoggedIn
                }
            })
            .when("/user/:userId/literature/:bookId/progress", {
                templateUrl: "views/progress/academia.progress.view.client.html",
                controller: "ProgressController",
                controllerAs: "model",
                resolve: {
                    loggedIn: checkLoggedIn
                }
            })
            .when("/user/:userId/group", {
                templateUrl: "views/group/academia.group_list.view.client.html",
                controller: "GroupListController",
                controllerAs: "model",
                resolve: {
                    loggedIn: checkLoggedIn
                }
            })
            .when("/user/:userId/literature/:bookId/group/:groupId/new_review", {
                templateUrl: "views/review/academia.new_review.view.client.html",
                controller: "NewReviewController",
                controllerAs: "model",
                resolve: {
                    loggedIn: checkLoggedIn
                }
            })
            .when("/user/:userId/literature/:bookId/group/:groupId/review/:reviewId", {
                templateUrl: "views/review/academia.edit_review.view.client.html",
                controller: "EditReviewController",
                controllerAs: "model",
                resolve: {
                    loggedIn: checkLoggedIn
                }
            })
            .when("/user/:userId/literature/:bookId/new_group", {
                templateUrl: "views/group/academia.new_group.view.client.html",
                controller: "NewGroupController",
                controllerAs: "model",
                resolve: {
                    loggedIn: checkLoggedIn
                }
            })
            .when("/user/:userId/literature/:bookId/group/:groupId", {
                templateUrl: "views/group/academia.group_discussion.view.client.html",
                controller: "GroupDiscussionController",
                controllerAs: "model",
                resolve: {
                    loggedIn: checkLoggedIn
                }
            })
            .when("/user/:userId/literature/:bookId/group/:groupId/config_group", {
                templateUrl: "views/group/academia.edit_group.view.client.html",
                controller: "GroupConfigController",
                controllerAs: "model",
                resolve: {
                    loggedIn: checkLoggedIn
                }
            })
            .otherwise({
                redirectTo: "/home"
            });


        function checkLoggedIn(UserService, $location, $q, $rootScope) {

            var deferred = $q.defer();

            UserService
                .loggedIn()
                .then(
                    function (response) {
                        var user = response.data;
                        if (user) {
                            $rootScope.currentUser = user;
                            deferred.resolve();
                        } else {
                            $rootScope.currentUser = null;
                            deferred.reject();
                            $location.url("/login");
                        }
                    },
                    function (err) {
                        $location.url("/home");
                    }
                );

            return deferred.promise;
        }

    }
})();




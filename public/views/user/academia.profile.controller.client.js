(function() {
    angular
        .module("HubAcademiaApp")
        .controller("ProfileController", ProfileController);

    function ProfileController($location, $rootScope, UserService) {
        var vm = this;
        vm.updateUser = updateUser;
        vm.logout = logout;

        var uid = $rootScope.currentUser._id;

        function init() {
            UserService
                .findUserById(uid)
                .then(function (response) {
                    vm.user = angular.copy(response.data);
                });
        }
        init();

        function logout() {
            UserService
                .logout()
                .then(
                    function () {
                        $location.url("/home");
                    },
                    function () {
                        $location.url("/home");
                    }
                );
        }

        function updateUser(newUser) {
            UserService
                .updateUser(uid, newUser)
                .then(
                    function (response) {
                        vm.success = response.data;
                    },
                    function (err) {
                        vm.error = err.data;
                    }
                );
        }
    }

})();

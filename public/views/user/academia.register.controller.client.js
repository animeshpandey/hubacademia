(function() {
    angular
        .module("HubAcademiaApp")
        .controller("RegisterController", RegisterController);

    function RegisterController ($location, UserService) {
        var vm = this;
        vm.createNewUser = function createNewUser(username, password, veri_password) {
            if (username) {
                if (password === veri_password) {
                    UserService
                        .register(username, password)
                        .then(
                            function (response) {
                                var user = response.data;
                                if (user) {
                                    $location.url("/user/" + user._id);
                                }
                            },
                            function (err) {
                                vm.error = err.data;
                            });
                }
                else {
                    vm.error = "Password not consistent.";
                }
            }
            else {
                vm.error = "Username should not be empty. ";
            }
        }
    }

})();
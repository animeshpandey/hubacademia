(function() {
    angular
        .module("HubAcademiaApp")
        .controller("LoginController", LoginController);

    function LoginController ($location, UserService) {
        var vm = this;
        vm.login = loginWithCredentials;

        function loginWithCredentials(username, password) {
            UserService
                .login(username, password)
                .then(
                    function (response) {
                        var user = response.data;
                        if (user && user._id) {
                            $location.url("/user/" + user._id);
                        } else {
                            vm.error = "User not found or wrong password ";
                        }
                    },
                    function () {
                        vm.error = "Username and password should not be empty. ";
                    });
        }
    }

})();
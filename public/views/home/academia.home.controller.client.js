(function() {
    angular
        .module("HubAcademiaApp")
        .controller("HomeController", HomeController);

    function HomeController($location, $routeParams, $timeout, GoogleBooksService) {
        var vm = this;
        vm.quickSearch = quickSearch;
        vm.getAuthors = getAuthors;
        vm.getTitle = getTitle;

        function quickSearch(text) {
            if (text) {
                GoogleBooksService
                    .quickSearch(text)
                    .then(
                        function (response) {
                            var resObj = GoogleBooksService.extractInfo(response.data);
                            vm.books = resObj.samples;
                            vm.totalNum = resObj.totalItems;
                        },
                        function () {
                            vm.error = "Google Books API failed. ";
                        }
                    );
            } else {
                return;
            }

        }

        function getAuthors(book) {
            return GoogleBooksService.getAuthors(book);
        }

        function getTitle(book) {
            return GoogleBooksService.getTitle(book);
        }

    }
})();

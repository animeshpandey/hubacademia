(function() {
    angular
        .module("HubAcademiaApp")
        .controller("ProgressController", ProgressController);

    function ProgressController ($routeParams, $location, UserService, BookDetailService, ProgressService) {
        var vm = this;
        vm.bookId = $routeParams.bookId;
        vm.userId = $routeParams.userId;

        vm.addRecord = addRecord;
        vm.removeRecord = removeRecord;
        vm.logout = logout;

        function init() {
            BookDetailService
                .findBookDetailByBookId(vm.bookId)
                .then(function (response) {
                    var book = response.data;
                    vm.pageCount = 1000;
                    if (book.hasOwnProperty('pageCount') && book.pageCount) {
                        vm.pageCount = book.pageCount;
                    }
                });
            ProgressService
                .findProgressByUserIdBookId(vm.userId, vm.bookId)
                .then(
                    function (response) {
                        var tmpProgress = response.data;
                        if (tmpProgress) {
                            vm.progress = tmpProgress;
                            var pageControl = vm.progress.progress;
                            var currentPage = 0;
                            if (pageControl.length) {
                                currentPage = pageControl[pageControl.length-1].page;
                            }
                            $( "#progressbar" ).progressbar({
                                value: 100 * currentPage / vm.pageCount
                            });
                        } else {
                            ProgressService
                                .createProgress(vm.userId, vm.bookId)
                                .then(
                                    function () {
                                        init();
                                    },
                                    function (err) {
                                        vm.error = err.data;
                                    }
                                );
                        }
                    },
                    function (err) {
                        vm.error = err.data;
                    }
                )

        }
        init();

        function logout() {
            UserService
                .logout()
                .then(
                    function () {
                        $location.url("/home");
                    },
                    function () {
                        $location.url("/home");
                    }
                );
        }

        function removeRecord(recordId) {
            ProgressService
                .removeRecord(vm.progress._id, recordId)
                .then(
                    function () {
                        init();
                    },
                    function (err) {
                        vm.error = err.data;
                    }
                );
        }

        function addRecord(date, chapter, page, remark) {
            ProgressService
                .addRecord(date, chapter, page, remark, vm.progress._id)
                .then(
                    function () {
                        init();
                    },
                    function (err) {
                        vm.error = err.data;
                    }
                );
        }


        
    }

})();
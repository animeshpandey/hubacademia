(function() {
    angular
        .module("HubAcademiaApp")
        .controller("MyBookController", MyBookController);

    function MyBookController ($location, $routeParams, UserService, MyBookService, BookDetailService, GoogleBooksService) {
        var vm = this;
        var bookIds= [];
        vm.userId = $routeParams.userId;

        vm.getAuthors = getAuthors;
        vm.getTitle = getTitle;
        vm.removeBook = removeBook;
        vm.logout = logout;

        function init() {
            MyBookService
                .findBooksByUserId(vm.userId)
                .then(
                    //first get the IDs of favorite books
                    function (response) {
                        bookIds = response.data;
                    },
                    function (err) {
                        vm.error = err.data;
                    }
                )
                .then(
                    // then get the details according to IDs
                    function () {
                        vm.books = [];
                        for (var i in bookIds) {
                            BookDetailService
                                .findBookDetailByBookId(bookIds[i].bookId)
                                .then(
                                    function (resp) {
                                        vm.books.push(resp.data);
                                    }
                                );
                        }
                    }
                );
        }
        init();

        function logout() {
            UserService
                .logout()
                .then(
                    function () {
                        $location.url("/home");
                    },
                    function () {
                        $location.url("/home");
                    }
                );
        }

        function getAuthors(book) {
            return GoogleBooksService.getAuthors(book);
        }

        function getTitle(book) {
            return GoogleBooksService.getTitle(book);
        }

        function removeBook(bookId) {
            MyBookService
                .removeBookById(vm.userId, bookId)
                .then(
                    function (response) {
                        init();
                        vm.success = response.data;

                    },
                    function (err) {
                        vm.error = err.data;
                    });
        }

    }

})();
(function() {
    angular
        .module("HubAcademiaApp")
        .controller("BookSearchController", BookSearchController);

    function BookSearchController ($routeParams, $location, UserService, BookDetailService, MyBookService, GoogleBooksService) {
        var vm = this;
        vm.userId = $routeParams.userId;

        vm.advancedSearch = advancedSearch;
        vm.getTitle = getTitle;
        vm.getAuthors = getAuthors;
        vm.followBook = followBook;
        vm.logout = logout;
        
        function followBook(book) {
            // if the literature is already in the database, use the data
            BookDetailService
                .findBookDetailByBookId(book.bookId)
                .then(
                    function (response) {
                        var bookInDB = response.data;
                        if (bookInDB) {
                            MyBookService
                                .findBookByBookIdWithUserId(vm.userId, bookInDB.bookId)
                                .then(
                                    function (response1) {
                                        var result = response1.data;
                                        if (result) {
                                            // if the literature is already in my literature list
                                            $location.url("/user/" + vm.userId + "/literature/" + result.bookId);
                                        } else {
                                            // else extract the data from database and add the literature into my list
                                            MyBookService
                                                .addBookFromDBtoMyBook(vm.userId, bookInDB.bookId)
                                                .then(
                                                    function () {
                                                        $location.url("/user/" + vm.userId + "/literature");
                                                    },
                                                    function (err) {
                                                        vm.error = err.data;
                                                    }
                                                );
                                        }
                                    }
                                );
                        } else {
                            // if the literature is not in the database yet, first add it into the database
                            BookDetailService
                                .createBookDetail(book)
                                .then(
                                    function () {
                                        // then add literature into my list
                                        MyBookService
                                            .addBookFromDBtoMyBook(vm.userId, book.bookId)
                                            .then(
                                                function () {
                                                    $location.url("/user/" + vm.userId + "/literature");
                                                },
                                                function (err) {
                                                    vm.error = err.data;
                                                }
                                            );
                                    },
                                    function (err) {
                                        vm.error = err.data;
                                    }
                                );
                        }
                    }
                );
        }

        function logout() {
            UserService
                .logout()
                .then(
                    function () {
                        $location.url("/home");
                    },
                    function () {
                        $location.url("/home");
                    }
                );
        }

        function getAuthors(book) {
            return GoogleBooksService.getAuthors(book);
        }

        function getTitle(book) {
            return GoogleBooksService.getTitle(book);
        }

        function advancedSearch(searchInfo) {
            if (searchInfo.keyWord) {
                if (searchInfo.maxNum) {
                    if (searchInfo.maxNum<=40 && searchInfo.maxNum>=1) {
                        GoogleBooksService
                            .advancedSearch(searchInfo)
                            .then(
                                function (response) {
                                    //console.log(response.data);
                                    vm.books = GoogleBooksService.extractInfo(response.data).samples;
                                    //console.log(vm.books);
                                },
                                function () {
                                    vm.error = "Google Books API failed. ";
                                }
                            );
                    } else {
                        return;
                    }

                } else {
                    GoogleBooksService
                        .advancedSearch(searchInfo)
                        .then(
                            function (response) {
                                vm.books = GoogleBooksService.extractInfo(response.data).samples;
                            },
                            function () {
                                vm.error = "Google Books API failed. ";
                            });
                }

            } else {
                return;
            }

        }


    }

})();
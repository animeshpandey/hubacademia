(function() {
    angular
        .module("HubAcademiaApp")
        .controller("BookDetailController", BookDetailController);

    function BookDetailController ($sce, $routeParams, $location, UserService, BookDetailService) {
        var vm = this;
        vm.bookId = $routeParams.bookId;
        vm.userId = $routeParams.userId;
        
        vm.getTitle = getTitle;
        vm.getAuthors = getAuthors;
        vm.getIndustrialIdentifier = getIndustrialIdentifier;
        vm.getSafeHtmlReview = getSafeHtmlReview;
        vm.logout = logout;

        function logout() {
            UserService
                .logout()
                .then(
                    function () {
                        $location.url("/home");
                    },
                    function () {
                        $location.url("/home");
                    }
                );
        }

        function init() {
            BookDetailService
                .findBookDetailByBookId(vm.bookId)
                .then(
                    function (response) {
                        vm.book = response.data;
                    },
                    function (err) {
                        vm.error = err.data;
                    }
                );
        }
        init();

        function getSafeHtmlReview() {
            return $sce.trustAsHtml(vm.review);
        }
        
        function getTitle(title, subtitle) {
            return BookDetailService.getFullTitle(title, subtitle);
        }

        function getAuthors(authors, lengthLimit) {
            return BookDetailService.getFullAuthors(authors, lengthLimit);
        }

        function getIndustrialIdentifier(identifier, type) {
            return BookDetailService.getIndustrialIdentifier(identifier, type);
        }
    }
})();

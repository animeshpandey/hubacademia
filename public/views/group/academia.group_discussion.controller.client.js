(function() {
    angular
        .module("HubAcademiaApp")
        .controller("GroupDiscussionController", GroupDiscussionController);

    function GroupDiscussionController ($sce, $routeParams, $location, UserService, ReviewService) {
        var vm = this;
        vm.userId = $routeParams.userId;
        vm.bookId = $routeParams.bookId;
        vm.groupId = $routeParams.groupId;
        vm.username = "anonymous";

        vm.getReview = getReview;
        vm.followUp = followUp;
        vm.deleteFollowUp = deleteFollowUp;
        vm.deleteReview = deleteReview;
        vm.logout = logout;

        function init() {
            ReviewService
                .findReviewsByGroupId(vm.groupId)
                .then(
                    function (response) {
                        vm.reviews = response.data;
                    },
                    function (err) {
                        vm.error = err.data;
                    }
                );
            UserService
                .findUserById(vm.userId)
                .then(
                    function (response) {
                        var user = response.data;
                        if (user) {
                            vm.username = user.username;
                        }
                    }
                );
        }
        init();

        function logout() {
            UserService
                .logout()
                .then(
                    function () {
                        $location.url("/home");
                    },
                    function () {
                        $location.url("/home");
                    }
                );
        }

        function deleteReview(reviewId) {
            ReviewService
                .deleteReview(reviewId)
                .then(
                    function () {
                        init();
                    },
                    function (err) {
                        vm.error = err.data;
                    }
                );
        }
        
        function deleteFollowUp(reviewIdFollowed, followUpId) {
            ReviewService
                .deleteFollowUp(reviewIdFollowed, followUpId)
                .then(
                    function () {
                        init();
                    },
                    function (err) {
                        vm.error = err.data;
                    }
                );
        }

        function getReview(htmlText) {
            return $sce.trustAsHtml(htmlText);
        }

        function followUp(reviewIdFollowed, text) {
            if (text) {
                ReviewService
                    .followUp(reviewIdFollowed, vm.username, text)
                    .then(
                        function () {
                            init();
                        },
                        function (err) {
                            vm.error = err.data;
                        }
                    );
            } else {
                return;
            }

        }

    }

})();
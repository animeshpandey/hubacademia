(function() {
    angular
        .module("HubAcademiaApp")
        .controller("NewGroupController", NewGroupController);

    function NewGroupController ($routeParams, $location, BookDetailService, UserService, GroupService) {
        var vm = this;
        vm.bookId = $routeParams.bookId;
        vm.userId = $routeParams.userId;
        vm.members = [];
        vm.bookTitle = "";
            
        vm.createGroup = createGroup;
        vm.getTitle = getTitle;
        vm.addMember = addMember;
        vm.removeTmpUser = removeTmpUser;
        vm.logout = logout;

        function init() {
            BookDetailService
                .findBookDetailByBookId(vm.bookId)
                .then(function (response) {
                    vm.book = response.data;
                    vm.bookTitle = getTitle(vm.book.title, vm.book.subtitle);
                });
            vm.members = [];
        }
        init();

        function logout() {
            UserService
                .logout()
                .then(
                    function () {
                        $location.url("/home");
                    },
                    function () {
                        $location.url("/home");
                    }
                );
        }

        function removeTmpUser(username) {
            for (var i in vm.members) {
                if (vm.members[i].username === username) {
                    vm.members.splice(i, 1);
                    return;
                }
            }
        }

        function usernameInTmpGroup(username) {
            for (var i in vm.members) {
                if (vm.members[i].username === username) {
                    return true;
                }
            }
            return false;
        }

        function addMember(username) {
            if (!usernameInTmpGroup(username)) {
                UserService
                    .findUserByUsername(username)
                    .then(
                        function (response) {
                            var user = response.data;
                            if (user) {
                                if (user._id === vm.userId) {
                                    return;
                                } else {
                                    vm.members.push(user);
                                    //console.log(vm.members);
                                }
                            } else {
                                return;
                            }
                        },
                        function (err) {
                            vm.error = err.data;
                        }
                    );
            } else {
                return;
            }
        }

        function getTitle(title, subtitle) {
            return BookDetailService.getFullTitle(title, subtitle);
        }

        function createGroup(groupName) {
            if (groupName) {
                GroupService
                    .createGroup(groupName, vm.userId, vm.members, vm.bookId, vm.bookTitle, vm.book.smallThumbnail)
                    .then(
                        function () {
                            $location.url("/user/" + vm.userId + "/group");
                        },
                        function (err) {
                            vm.error = err.data;
                        }
                    );
            } else {
                return;
            }

        }
        
    }

})();
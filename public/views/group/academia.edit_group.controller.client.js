(function() {
    angular
        .module("HubAcademiaApp")
        .controller("GroupConfigController", GroupConfigController);

    function GroupConfigController ($routeParams, $location, UserService, GroupService) {
        var vm = this;
        vm.userId = $routeParams.userId;
        vm.bookId = $routeParams.bookId;
        vm.groupId = $routeParams.groupId;
        vm.members = [];

        vm.addMember = addMember;
        vm.updateGroup = updateGroup;
        vm.removeTmpUser = removeTmpUser;
        vm.logout = logout;

        function init() {
            GroupService
                .findGroupByGroupId(vm.groupId)
                .then(
                    function (response) {
                        vm.group = response.data;
                        vm.members = vm.group.members;
                    },
                    function (err) {
                        vm.error = err.data;
                    }
                );

        }
        init();

        function logout() {
            UserService
                .logout()
                .then(
                    function () {
                        $location.url("/home");
                    },
                    function () {
                        $location.url("/home");
                    }
                );
        }

        function updateGroup() {
            if (vm.group.groupName) {
                vm.group.members = vm.members;
                GroupService
                    .updateGroup(vm.group)
                    .then(
                        function () {
                            $location.url("/user/" + vm.userId + "/group");
                        },
                        function (err) {
                            vm.error = err.data;
                        }
                    );
            } else {
                return;
            }

        }

        function removeTmpUser(username) {
            for (var i in vm.members) {
                if (vm.members[i].username === username) {
                    vm.members.splice(i, 1);
                    return;
                }
            }
            return;
        }

        function usernameInTmpGroup(username) {
            for (var i in vm.members) {
                if (vm.members[i].username === username) {
                    return true;
                }
            }
            return false;
        }

        function addMember(username) {
            if (!usernameInTmpGroup(username)) {
                UserService
                    .findUserByUsername(username)
                    .then(
                        function (response) {
                            var user = response.data;
                            if (user) {
                                if (user._id === vm.userId) {
                                    return;
                                } else {
                                    vm.members.push(user);
                                    //console.log(vm.members);
                                }
                            } else {
                                return;
                            }
                        },
                        function (err) {
                            vm.error = err.data;
                        }
                    );
            } else {
                return;
            }
        }
        



    }

})();
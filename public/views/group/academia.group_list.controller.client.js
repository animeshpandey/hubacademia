(function() {
    angular
        .module("HubAcademiaApp")
        .controller("GroupListController", GroupListController);

    function GroupListController ($routeParams, $location, BookDetailService, UserService, GroupService) {
        var vm = this;
        vm.userId = $routeParams.userId;

        vm.dismissGroup = dismissGroup;
        vm.leaveGroup = leaveGroup;
        vm.logout = logout;

        function leaveGroup(groupId) {
            GroupService
                .leaveGroup(vm.userId, groupId)
                .then(
                    function () {
                        init();
                    },
                    function (err) {
                        vm.error = err.data;
                    }
                );
        }

        function logout() {
            UserService
                .logout()
                .then(
                    function () {
                        $location.url("/home");
                    },
                    function () {
                        $location.url("/home");
                    }
                );
        }

        function dismissGroup(groupId) {
            GroupService
                .dismissGroup(groupId)
                .then(
                    function () {
                        init();
                    },
                    function (err) {
                        vm.error = err.data;
                    }
                );
        }

        function init() {
            GroupService
                .findMyOwnedGroupsByUserId(vm.userId)
                .then(
                    function (response) {
                        vm.myOwnedGroups = response.data;
                        //console.log(vm.myOwnedGroups);
                    },
                    function (err) {
                        vm.error = err.data;
                    }
                );
            GroupService
                .findGroupsIamIn(vm.userId)
                .then(
                    function (response) {
                        vm.groupsIamIn = response.data;
                    },
                    function (err) {
                        vm.error = err.data;
                    }
                );

        }
        init();
        
    }

})();
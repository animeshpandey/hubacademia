(function() {
    angular
        .module("HubAcademiaApp")
        .controller("NewReviewController", NewReviewController);

    function NewReviewController ($location, $routeParams, UserService, ReviewService) {
        var vm = this;
        vm.userId = $routeParams.userId;
        vm.bookId = $routeParams.bookId;
        vm.groupId = $routeParams.groupId;
        vm.username = "anonymous";

        vm.createNewReview = createNewReview;
        vm.logout = logout;

        function init() {
            UserService
                .findUserById(vm.userId)
                .then(
                    function (response) {
                        var user = response.data;
                        if (user) {
                            vm.username = user.username;
                        }
                    }
                );
        }
        init();

        function logout() {
            UserService
                .logout()
                .then(
                    function () {
                        $location.url("/home");
                    },
                    function () {
                        $location.url("/home");
                    }
                );
        }
        
        function createNewReview(rating, review) {
            if (review) {
                ReviewService
                    .createNewReview(vm.userId, vm.username, vm.bookId, vm.groupId, review, rating)
                    .then(
                        function () {
                            $location.url("/user/" + vm.userId+ "/literature/" + vm.bookId + "/group/" + vm.groupId);
                        },
                        function (err) {
                            vm.error = err.data;
                        }
                    );
            } else {
                return;
            }

        }

    }
})();
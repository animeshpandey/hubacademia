(function() {
    angular
        .module("HubAcademiaApp")
        .controller("EditReviewController", EditReviewController);

    function EditReviewController ($location, $routeParams, ReviewService, UserService) {
        var vm = this;
        vm.userId = $routeParams.userId;
        vm.bookId = $routeParams.bookId;
        vm.groupId = $routeParams.groupId;
        vm.reviewId = $routeParams.reviewId;

        vm.updateReview = updateReview;
        vm.deleteReview = deleteReview;
        vm.logout = logout;

        function init() {
            ReviewService
                .findReviewById(vm.reviewId)
                .then(
                    function (response) {
                        vm.review = response.data;
                    }
                );
        }
        init();

        function updateReview(newReview) {
            if (newReview.review) {
                ReviewService
                    .updateReview(vm.reviewId, newReview)
                    .then(
                        function () {
                            $location.url("/user/" + vm.userId + "/literature/" + vm.bookId + "/group/" + vm.groupId);
                        },
                        function (err) {
                            vm.error = err.data;
                        }
                    )
            } else {
                return;
            }

        }

        function logout() {
            UserService
                .logout()
                .then(
                    function () {
                        $location.url("/home");
                    },
                    function () {
                        $location.url("/home");
                    }
                );
        }



        function deleteReview() {
            ReviewService
                .deleteReview(vm.reviewId)
                .then(
                    function () {
                        $location.url("/user/" + vm.userId + "/literature/" + vm.bookId + "/group/" + vm.groupId);
                    },
                    function (err) {
                        vm.error = err.data;
                    }
                );
        }

    }

})();
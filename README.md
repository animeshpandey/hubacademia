HubAcademia
------------

This application is a mini-forum that could be used by academic researchers to give insights and reviews about the research or academic content available online. This application utilizes Google Books API for retrieving information of the journals or books.

This application has been hosted on Heroku running on a single dyno and using a free instance of *mLab mongoDB*.

The application is hosted [here](http://hub-academia.herokuapp.com).

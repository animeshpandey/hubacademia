// project user-book relation service server
module.exports = function(app, models) {

    var reviewModel = models.reviewModel;

    app.post("/api/review", createNewReview);
    app.post("/api/review/:reviewId/followup", followUp);
    app.get("/api/group/:groupId/review", findReviewsByGroupId);
    app.get("/api/review/:reviewId", findReviewById);
    app.put("/api/review/:reviewId", updateReview);
    app.delete("/api/review/:reviewId/followup/:followupId", deleteFollowUp);
    app.delete("/api/review/:reviewId", deleteReview);

    
    function updateReview(req, res) {
        var reviewId = req.params.reviewId;
        var newReview = req.body;

        reviewModel
            .updateReview(reviewId, newReview)
            .then(
                function () {
                    res.sendStatus(200);
                },
                function () {
                    res.status(400).send("Update failed. ");
                }
            );
    }
    
    function findReviewById(req, res) {
        var reviewId = req.params.reviewId;

        reviewModel
            .findReviewById(reviewId)
            .then(
                function (response) {
                    res.json(response);
                },
                function () {
                    res.status(404).send("Failed to search for review. ")
                }
            );

    }

    function deleteReview(req, res) {
        var reviewId = req.params.reviewId;

        reviewModel
            .deleteReview(reviewId)
            .then(
                function () {
                    res.sendStatus(200);
                },
                function () {
                    res.status(400).send("Failed to delete review. ");
                }
            );
    }

    function deleteFollowUp(req, res) {
        var reviewId = req.params.reviewId;
        var followupId = req.params.followupId;

        reviewModel
            .deleteFollowUp(reviewId, followupId)
            .then(
                function () {
                    res.sendStatus(200);
                },
                function () {
                    res.status(404).send("Failed to delete the followup. ");
                }
            );

    }

    function followUp(req, res) {
        var reviewId = req.params.reviewId;
        var newFollowUp = req.body;

        reviewModel
            .followUp(reviewId, newFollowUp)
            .then(
                function () {
                    res.sendStatus(200);
                },
                function () {
                    res.status(404).send("Failed to follow up. ");
                }
            );
        
    }
    
    function createNewReview(req, res) {
        var newReview = req.body;

        reviewModel
            .createNewReview(newReview)
            .then(
                function () {
                    res.sendStatus(200);
                },
                function () {
                    res.sendStatus(400).send("Failed to create new review. ")
                }
            );
    }

    function findReviewsByGroupId(req, res) {
        var groupId = req.params.groupId;

        reviewModel
            .findReviewsByGroupId(groupId)
            .then(
                function (response) {
                    res.send(response);
                },
                function (err) {
                    res.sendStatus(404).send("Failed to search for review. ");
                }
            );
    }




};
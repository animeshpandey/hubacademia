// project user-book relation service server
module.exports = function(app, models) {
    var myBookModel = models.myBookModel;
    
    app.post("/api/bookshelf_user/:userId/literature", createBook);
    app.get("/api/bookshelf_user/:userId/literature", findBooksByUserId);
    app.get("/api/bookshelf_user/:userId/literature/:bookId", findBookByBookIdWithUserId);
    app.put("/api/bookshelf_user/:userId/literature/:bookId", updateMyBook);
    app.delete("/api/bookshelf_user/:userId/literature/:bookId", removeBookById);

    function updateMyBook(req, res) {
        var userId = req.params.userId;
        var bookId = req.params.bookId;
        var newMyBook = req.body;

        myBookModel
            .updateMyBook(userId, bookId, newMyBook)
            .then(
                function () {
                    res.sendStatus(200);
                },
                function () {
                    res.status(404).send("Update failed. ");
                }
            );
    }

    function createBook(req, res) {
        var userId = req.params.userId;
        var book = req.body;

        myBookModel
            .createBook(book)
            .then(
                function () {
                    res.sendStatus(200);
                },
                function () {
                    res.status(400).send("Unable to add to your favourite. ");
                }
            );
    }

    function findBookByBookIdWithUserId(req, res) {
        var userId = req.params.userId;
        var bookId = req.params.bookId;

        myBookModel
            .findBookByBookIdWithUserId(userId, bookId)
            .then(
                function (book) {
                    res.json(book);
                },
                function () {
                    res.sendStatus(404);
                }
            );
    }

    function findBooksByUserId(req, res) {
        var userId = req.params.userId;
        myBookModel
            .findBooksByUserId(userId)
            .then(
                function (response) {
                    res.send(response);
                },
                function () {
                    res.status(404).send("Failed to load your favorite books. ");
                }
            );
    }

    function removeBookById(req, res) {
        var userId = req.params.userId;
        var bookId = req.params.bookId;
        myBookModel
            .removeBookById(userId, bookId)
            .then(
                function () {
                    res.status(200).send("Book removed. ");
                },
                function () {
                    res.status(404).send("Unable to remove the literature. ");
                }
            )
    }

};
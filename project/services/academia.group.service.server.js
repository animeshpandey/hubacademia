// project book detail service server
module.exports = function(app, models) {
    var groupModel = models.groupModel;

    app.post("/api/newgroup", createGroup);
    app.get("/api/group", findGroup);
    app.put("/api/group/:groupId", updateGroup);
    app.delete("/api/group/:groupId", dismissGroup);
    app.delete("/api/user/:userId/group/:groupId", leaveGroup);

    function leaveGroup(req, res) {
        var userId = req.params.userId;
        var groupId = req.params.groupId;

        groupModel
            .leaveGroup(userId, groupId)
            .then(
                function () {
                    res.sendStatus(200);
                },
                function () {
                    res.status(404).send("Unable to leave the group. ")
                }
            );

    }

    function updateGroup(req, res) {
        var groupId = req.params.groupId;
        var newGroup = req.body;

        groupModel
            .updateGroup(groupId, newGroup)
            .then(
                function () {
                    res.sendStatus(200);
                },
                function () {
                    res.status(400).send("Failed to update the group. ");
                }
            );
    }

    function dismissGroup(req, res) {
        var groupId = req.params.groupId;
        groupModel
            .dismissGroup(groupId)
            .then(
                function () {
                    res.sendStatus(200);
                },
                function () {
                    res.status(400).send("Failed to dismiss the group. ");
                }
            );

    }

    function findMyOwnedGroupByUserId(req, res) {
        var userId = req.params.userId;

        groupModel
            .findMyOwnedGroupByUserId(userId)
            .then(
                function (response) {
                    res.send(response);
                },
                function () {
                    res.status(404).send("Failed to search. ");
                }
            );
    }

    function createGroup(req, res) {
        var newGroup = req.body;

        groupModel
            .createGroup(newGroup)
            .then(
                function () {
                    res.sendStatus(200);
                },
                function (err) {
                    res.status(400).send(err);
                }
            );
    }
    

    function findGroup(req, res) {
        var leaderId = req.query['leaderId'];
        var bookId = req.query['bookId'];
        var memberId = req.query['memberId'];
        var groupId = req.query['groupId'];

        if (groupId) {
            groupModel
                .findGroupByGroupId(groupId)
                .then(
                    function (response) {
                        res.json(response);
                    },
                    function () {
                        res.status(404).send("Failed to search. ");
                    }
                );
        }

        if (leaderId) {
            if (bookId) {
                groupModel
                    .findMyOwnedGroup(leaderId, bookId)
                    .then(
                        function (response) {
                            res.json(response);
                        },
                        function () {
                            res.status(404).send("Failed to search. ");
                        }
                    );
            } else {
                groupModel
                    .findMyOwnedGroupsByUserId(leaderId)
                    .then(
                        function (response) {
                            res.send(response);
                        },
                        function () {
                            res.status(404).send("Failed to search. ");
                        }
                    );
            }
        }
        
        if (memberId) {
            if (bookId) {
                groupModel
                    .findMyOwnedGroupsByUserId(leaderId)
                    .then(
                        function (response) {
                            res.send(response);
                        },
                        function () {
                            res.status(404).send("Failed to search. ");
                        }
                    );
            } else {
                groupModel
                    .findGroupsIamIn(memberId)
                    .then(
                        function (response) {
                            res.send(response);
                        },
                        function () {
                            res.status(404).send("Failed to search. ");
                        }
                    );
            }

        }

    }

};
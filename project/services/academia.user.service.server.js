// project user service server
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
var bcrypt = require("bcrypt-nodejs");


module.exports = function(app, models) {
    var userModel = models.userModel;
    
    app.post("/api/bookshelf_user", createUser);
    app.post("/api/bookshelf_user_logout", logout);
    app.post("/api/bookshelf_user_login", passport.authenticate('bookshelf'), login);
    app.post("/api/bookshelf_user_register", register);

    app.get('/auth/google', passport.authenticate('google', { scope : ['profile', 'email'] }));
    app.get('/auth/google/callback',
        passport.authenticate('google', {
            successRedirect: '/project/#/user',
            failureRedirect: '/project/#/login'
        }));
    app.get("/api/bookshelf_user_loggedIn", loggedIn);
    app.get("/api/bookshelf_user", getUsers);
    app.get("/api/bookshelf_user/:userId", findUserById);

    app.put("/api/bookshelf_user/:userId", updateUser);

    passport.use('bookshelf', new LocalStrategy(BookShelfLocalStrategy));
    passport.serializeUser(serializeUser);
    passport.deserializeUser(deserializeUser);

    function BookShelfLocalStrategy(username, password, done) {
        userModel
            .findUserByUsername(username)
            .then(
                function(user){
                    if(user && bcrypt.compareSync(password, user.password)) {
                        done(null, user);
                    } else {
                        done(null, false);
                    }
                },
                function(err) {
                    done(err);
                }
            );
    }

    var googleConfig = {
        clientID     : process.env.GOOGLE_CLIENT_ID,
        clientSecret : process.env.GOOGLE_CLIENT_SECRET,
        callbackURL  : process.env.GOOGLE_CALLBACK_URL
    };

    passport.use("google", new GoogleStrategy(googleConfig, googleLogin));

    function googleLogin(token, refreshToken, profile, done) {
        userModel
            .findUserByGoogleId(profile.id)
            .then(
                function(user) {
                    if(user) {
                        return done(null, user);
                    } else {
                        var email = profile.emails[0].value;
                        var emailParts = email.split("@");
                        var newGoogleUser = {
                            username:  emailParts[0],
                            firstName: profile.name.givenName,
                            lastName:  profile.name.familyName,
                            email:     email,
                            google: {
                                id:    profile.id,
                                token: token
                            }
                        };
                        return userModel
                            .createUser(newGoogleUser);
                    }
                },
                function(err) {
                    if (err) { return done(err); }
                }
            )
            .then(
                function(user){
                    return done(null, user);
                },
                function(err){
                    if (err) { return done(err); }
                }
            );
    }

    function register(req, res) {
        var username = req.body.username;
        var password = req.body.password;

        userModel
            .findUserByUsername(username)
            .then(
                function (user) {
                    if (user) {
                        res.status(400).send("Username already exists.");
                    } else {
                        req.body.password = bcrypt.hashSync(req.body.password);
                        userModel
                            .createUser(req.body)
                            .then(
                                function (user) {
                                    if (user) {
                                        req.login(user, function (err) {
                                            if (err) {
                                                res.status(400).send("Failed to login with new user. ");
                                            } else {
                                                res.json(user);
                                            }
                                        })
                                    }
                                },
                                function () {
                                    res.status(400).send("Failed to create new user. ");
                                }
                            );
                    }
                },
                function (err) {
                    res.status(400).send(err);
                }
            );

    }

    function logout(req, res) {
        req.logout();
        res.sendStatus(200);
    }
    
    function login(req, res) {
        var user = req.user;
        res.json(user);
    }

    function loggedIn(req, res) {
        if (req.isAuthenticated()) {
            res.json(req.user);
        } else {
            res.send(null);
        }
    }

    function serializeUser(user, done) {
        done(null, user);
    }

    function deserializeUser(user, done) {
        userModel
            .findUserById(user._id)
            .then(
                function(user){
                    done(null, user);
                },
                function(err){
                    done(err, null);
                }
            );
    }

    function createUser(req, res) {
        var newUser = req.body;
        userModel
            .createUser(newUser)
            .then(
                function (user) {
                    res.json(user);
                },
                function (error) {
                    res.json(error);
                }
            );
    }

    function getUsers(req, res) {
        var username = req.query['username'];
        var password = req.query['password'];
        if (username && password) {
            return findUserByCredentials(username, password, req, res);
        } else if (username) {
            return findUserByUsername(username, res);
        }
        res.sendStatus(400);
    }

    function findUserByUsername(username, res) {
        userModel
            .findUserByUsername(username)
            .then(
                function (user) {
                    res.json(user);
                },
                function (error) {
                    res.status(404).send(error);
                }
            );
    }

    function findUserByCredentials(username, password, req, res) {
        userModel
            .findUserByCredentials(username, password)
            .then(
                function (user) {
                    req.session.currentUser = user;
                    res.json(user);
                },
                function () {
                    res.status(404).send("User not found or wrong password");
                }
            );
    }

    function findUserById(req, res) {
        var id = req.params.userId;
        userModel
            .findUserById(id)
            .then(
                function (user) {
                    res.json(user);
                },
                function (error) {
                    res.status(404).send(error);
                }
            );
    }

    function updateUser(req, res) {
        var id = req.params.userId;
        var newUser = req.body;
        userModel
            .updateUser(id, newUser)
            .then(
                function () {
                    res.status(200).send("User updated. ");
                },
                function () {
                    res.status(404).send("Update failed. ");
                }
            );
    }

    function deleteUser(req, res) {
        var userId = req.params.userId;

        userModel
            .deleteUser(userId)
            .then(
                function (user) {
                    res.sendStatus(200);
                },
                function (error) {
                    res.status(404).send(error);
                }
            );
    }
};

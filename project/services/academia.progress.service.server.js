// project user-book relation service server
module.exports = function(app, models) {

    var progressModel = models.progressModel;

    app.post("/api/progress", createProgress);
    app.put("/api/progress/:progressId", addRecord);
    app.delete("/api/progress/:progressId/record/:recordId", removeRecord);
    app.get("/api/bookshelf_user/:userId/literature/:bookId/progress", findProgressByUserIdBookId);

    function removeRecord(req, res) {
        var recordId = req.params.recordId;
        var progressId = req.params.progressId;

        progressModel
            .removeRecord(progressId, recordId)
            .then(
                function () {
                    res.sendStatus(200);
                },
                function () {
                    res.status(404).send("Failed to remove the record. ");
                }
            );

    }

    function addRecord(req, res) {
        var newRecord = req.body;
        var progressId = req.params.progressId;

        progressModel
            .addRecord(progressId, newRecord)
            .then(
                function () {
                    res.sendStatus(200);
                },
                function () {
                    res.status(404).send("Failed to add new record. ");
                }
            );
    }

    function createProgress(req, res) {
        var newProgress = req.body;

        progressModel
            .createProgress(newProgress)
            .then(
                function () {
                    res.sendStatus(200);
                },
                function () {
                    res.status(400).send("Failed to create new progress. ");
                }
            );
    }
    
    function findProgressByUserIdBookId(req, res) {
        var userId = req.params.userId;
        var bookId = req.params.bookId;

        progressModel
            .findProgressByUserIdBookId(userId, bookId)
            .then(
                function (response) {
                    res.json(response);
                },
                function (err) {
                    res.status(400).send(err);
                }
            );
    }
};

module.exports = function(app, models) {
    var bookDetailModel = models.bookDetailModel;

    app.post("/api/book_detail", createBookDetail);
    app.get("/api/book_detail/:bookId", findBookDetailByBookId);

    function createBookDetail(req, res) {
        var newBook = req.body;

        bookDetailModel
            .createBookDetail(newBook)
            .then(
                function () {
                    res.sendStatus(200);
                },
                function () {
                    res.status(400)
                        .send("Failed to create Book Detail record. ");
                }
            );
    }
    

    function findBookDetailByBookId(req, res) {
        var bookId = req.params.bookId;

        bookDetailModel
            .findBookDetailByBookId(bookId)
            .then(
                function (response) {
                    res.json(response);
                },
                function () {
                    res.status(404)
                        .send("Book not found. ");
                }
            );
    }

};
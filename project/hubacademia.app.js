module.exports = function(app) {
    var models = require("./models/academia.models.server.js")();

    require("./services/academia.user.service.server.js")(app, models);
    require("./services/academia.my_literature.service.server.js")(app, models);
    require("./services/academia.literature_detail.service.server.js")(app, models);
    require("./services/academia.group.service.server.js")(app, models);
    require("./services/academia.review.service.server.js")(app, models);
    require("./services/academia.progress.service.server.js")(app, models);
};
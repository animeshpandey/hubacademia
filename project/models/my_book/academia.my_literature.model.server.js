module.exports = function () {

    var mongoose = require("mongoose");
    var myBookSchema = require("./academia.my_literature.schema.server.js")();
    var myBook = mongoose.model("myBook", myBookSchema);

    function updateMyBook(userId, bookId, newMyBook) {
        delete newMyBook._id;
        delete newMyBook.bookId;
        delete newMyBook.ownerId;
        return myBook.update({
            ownerId: userId,
            bookId: bookId
        },{
            $set: newMyBook
        });
    }

    function createBook(book) {
        return myBook.create(book);
    }
    
    function findBookByBookIdWithUserId(userId, bookId) {
        return myBook.findOne({
            ownerId: userId,
            bookId: bookId
        });
    }

    function findBooksByUserId(userId) {
        return myBook.find({
            ownerId: userId
        });
    }
    
    function removeBookById(userId, bookId) {
        return myBook.remove({
            ownerId: userId,
            bookId: bookId
        });
    }

    var api = {
        findBooksByUserId: findBooksByUserId,
        removeBookById: removeBookById,
        findBookByBookIdWithUserId: findBookByBookIdWithUserId,
        createBook: createBook,
        updateMyBook: updateMyBook
    };
    return api;
};
module.exports = function () {
    var mongoose = require("mongoose");

    var myBookSchema = mongoose.Schema({
        ownerId: {
            type: mongoose.Schema.ObjectId,
            ref: "bookShelfUser"
        },
        bookId: String,
        dateCreated: {
            type: Date,
            default: Date.now
        }
    }, {
        collection: "bookshelf.my_book"
    });

    return myBookSchema;
};

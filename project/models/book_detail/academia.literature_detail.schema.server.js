module.exports = function () {
    var mongoose = require("mongoose");

    var bookDetailSchema = mongoose.Schema({
        bookId: String,
        title: String,
        subtitle: String,
        publisher: String,
        publishedDate: String,
        description: String,
        authors: [String],
        pageCount: Number,
        identifier: String,
        identifierType: String,
        previewLink: String,
        smallThumbnail: String,
        dateCreated: {
            type: Date,
            default: Date.now
        }}, {
            collection: "bookshelf.book_detail"
        }
    );

    return bookDetailSchema;
};

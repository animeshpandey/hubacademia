module.exports = function () {

    var mongoose = require("mongoose");
    var bookDetailSchema = require("./academia.literature_detail.schema.server.js")();
    var bookDetail = mongoose.model("bookDetail", bookDetailSchema);

    function createBookDetail(newBook) {
        return bookDetail.create(newBook);
    }

    function findBookDetailByBookId(bookId) {
        return bookDetail.findOne({bookId: bookId});
    }

    var api = {
        findBookDetailByBookId: findBookDetailByBookId,
        createBookDetail: createBookDetail
    };
    return api;
};
module.exports = function () {
    var mongoose = require("mongoose");

    var connectionString = 'mongodb://127.0.0.1:27017/BookShelfDatabase';

    if (process.env.MONGODB_URI) {
        connectionString = process.env.MONGODB_URI + "/BookShelfDatabase";
    }

    mongoose.connect(connectionString, function (err, res) {
        console.log(res);
        if (err) {
            console.log ('ERROR connecting to: ' + connectionString + '. ' + err);
        } else {
            console.log ('Succeeded connected to: ' + connectionString);
        }
    });

    console.log(connectionString);
    console.log("Activating project db");

    var models = {
        userModel: require("./user/academia.user.model.server.js")(),
        myBookModel: require("./my_book/academia.my_literature.model.server.js")(),
        bookDetailModel: require("./book_detail/academia.literature_detail.model.server.js")(),
        groupModel: require("./group/academia.group.model.server.js")(),
        reviewModel: require("./review/academia.review.model.server.js")(),
        followUpModel: require("./review/academia.followup.model.server.js")(),
        progressModel: require("./progress/academia.progress.model.server.js")()
    };
    return models;
};

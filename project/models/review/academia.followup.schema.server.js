module.exports = function () {
    var mongoose = require("mongoose");

    var followUpSchema = mongoose.Schema({
        reviewIdFollowed: {
            type: mongoose.Schema.ObjectId,
            ref: "BookShelfReview"
        },
        username: String,
        comment: String,
        dateCreated: {
            type: Date,
            default: Date.now
        }
    }, {
        collection: "bookshelf.followup"
    });

    return followUpSchema;
};

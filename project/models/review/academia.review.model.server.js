module.exports = function () {
    var mongoose = require("mongoose");
    var reviewSchema = require("./academia.review.schema.server.js")();
    var review = mongoose.model("BookShelfReview", reviewSchema);

    function followUp(reviewId, newFollowUp) {
        return review.update({
            _id: reviewId
        }, {
            $push: {
                followUps: newFollowUp
            }
        });
    }

    function deleteFollowUp(reviewId, followupId) {
        return review.update({
            _id: reviewId
        }, {
            $pull: {
                followUps: {
                    _id: followupId
                }
            }
        });
    }

    function findReviewById(reviewId) {
        return review.findOne({
            _id: reviewId
        });
    }
    
    function deleteReview(reviewId) {
        return review.remove({
            _id: reviewId
        });
    }

    function findReviewsByGroupId(groupId) {
        return review.find({
            groupId: groupId
        });
    }

    function updateReview(reviewId, newReview) {
        return review.update({
            _id: reviewId
        },{
            $set: {
                review: newReview.review,
                rating: newReview.rating
            }
        });
    }

    function createNewReview(newReview) {
        return review.create(newReview);
    }

    var api = {
        findReviewsByGroupId: findReviewsByGroupId,
        findReviewById: findReviewById,
        createNewReview: createNewReview,
        updateReview: updateReview,
        deleteReview: deleteReview,
        deleteFollowUp: deleteFollowUp,
        followUp: followUp
    };
    return api;
};
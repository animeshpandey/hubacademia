module.exports = function () {
    var mongoose = require("mongoose");
    var followUpSchema = require("./academia.followup.schema.server.js")();

    var reviewSchema = mongoose.Schema({
        reviewerId: {
            type: mongoose.Schema.ObjectId,
            ref: "bookShelfUser"
        },
        reviewerName: String,
        bookId: String,
        groupId: String,
        review: String,
        rating: String,
        followUps: [followUpSchema],
        dateCreated: {
            type: Date,
            default: Date.now
        }
    }, {
        collection: "bookshelf.review"
    });

    return reviewSchema;
};

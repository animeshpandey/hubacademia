module.exports = function () {
    var mongoose = require("mongoose");

    var progressRecordSchema = mongoose.Schema({
        date: Date,
        chapter: String,
        page: Number,
        remark: String,
        dateCreated: {
            type: Date,
            default: Date.now
        }
    }, {
        collection: "bookshelf.record"
    });

    return progressRecordSchema;
};

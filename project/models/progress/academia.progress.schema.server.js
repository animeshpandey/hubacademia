module.exports = function () {
    var mongoose = require("mongoose");

    var recordSchema = require("./academia.progress_record.schema.server.js")();

    var progressSchema = mongoose.Schema({
        ownerId: {
            type: mongoose.Schema.ObjectId,
            ref: "bookShelfUser"
        },
        bookId: String,
        progress: [recordSchema],
        dateCreated: {
            type: Date,
            default: Date.now
        }
    }, {
        collection: "bookshelf.progress"
    });

    return progressSchema;
};
module.exports = function () {

    var mongoose = require("mongoose");
    var progressSchema = require("./academia.progress.schema.server.js")();
    var progress = mongoose.model("progress", progressSchema);

    function removeRecord(progressId, recordId) {
        return progress.update({
            _id: progressId
        }, {
            $pull: {
                progress: {
                    _id: recordId
                }
            }
        });
    }

    function addRecord(progressId, newRecord) {
        return progress.update({
            _id: progressId
        }, {
            $push: {
                progress: newRecord
            }
        });
    }

    function findProgressByUserIdBookId(userId, bookId) {
        return progress.findOne({
            ownerId: userId,
            bookId: bookId
        });
    }

    function createProgress(newProgress) {
        return progress.create(newProgress);
    }

    var api = {
        findProgressByUserIdBookId: findProgressByUserIdBookId,
        createProgress: createProgress,
        addRecord: addRecord,
        removeRecord: removeRecord
    };
    return api;
};

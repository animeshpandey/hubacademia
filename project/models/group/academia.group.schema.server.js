module.exports = function () {
    var mongoose = require("mongoose");

    var groupSchema = mongoose.Schema({
        groupName: String,
        leaderId: {
            type: mongoose.Schema.ObjectId,
            ref: "bookShelfUser"
        },
        bookId: String,
        bookTitle: String,
        bookSmallThumbnail: String,
        members: [],
        dateCreated: {
            type: Date,
            default: Date.now
        }
    }, {
        collection: "bookshelf.bookshelf_group"
    });

    return groupSchema;
};

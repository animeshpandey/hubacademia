module.exports = function () {

    var mongoose = require("mongoose");
    var groupSchema = require("./academia.group.schema.server.js")();
    var groupManager = mongoose.model("groupManager", groupSchema);

    function leaveGroup(userId, groupId) {
        return groupManager.update({
            _id: groupId
        }, {
            $pull: {
                members: {
                    _id: userId
                }
            }
        });
    }

    function updateGroup(groupId, newGroup) {
        delete newGroup._id;
        return groupManager.update({
            _id: groupId
        }, {
            $set: newGroup
        });
    }

    function findGroupByGroupId(groupId) {
        return groupManager.findOne({
            _id: groupId
        });
    }

    function dismissGroup(groupId) {
        return groupManager.remove({
            _id: groupId
        });
    }

    function findGroupsIamIn(memberId) {
        return groupManager.find({
            members: {
                $elemMatch: {
                    _id: memberId
                }
            }
        });
    }

    function findMyOwnedGroupsByUserId(leaderId) {
        return groupManager.find({
            leaderId: leaderId
        });
    }
    
    function createGroup(newGroup) {
        return groupManager.create(newGroup);
    }

    function findMyOwnedGroup(leaderId, bookId) {
        return groupManager.findOne({
            leaderId: leaderId,
            bookId: bookId
        });
    }

    var api = {
        createGroup: createGroup,
        findMyOwnedGroup: findMyOwnedGroup,
        findMyOwnedGroupsByUserId: findMyOwnedGroupsByUserId,
        findGroupsIamIn: findGroupsIamIn,
        dismissGroup: dismissGroup,
        findGroupByGroupId: findGroupByGroupId,
        updateGroup: updateGroup,
        leaveGroup: leaveGroup
    };
    return api;
};
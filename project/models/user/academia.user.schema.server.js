module.exports = function () {
    var mongoose = require("mongoose");
    var uniqueValidator = require('mongoose-unique-validator');

    var bookShelfUserSchema = mongoose.Schema({
        username: {type: String, required: true, unique: true},
        password: String,
        firstName: String,
        lastName: String,
        email: String,
        google: {
            id: String,
            token: String
        },
        favoriteType: String,
        dateCreated: {type: Date, default: Date.now}
    }, {
        collection: "bookshelf.user"
    });

    bookShelfUserSchema.plugin(uniqueValidator);
    return bookShelfUserSchema;
};

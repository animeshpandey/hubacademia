var express = require('express');
var favicon = require('serve-favicon');
var passport = require('passport');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session      = require('express-session');

var app = express();

var SESSION_SECRET = process.env.SESSION_SECRET;

app.use(cookieParser());
app.use(session({secret: SESSION_SECRET}));

app.use(passport.initialize());
app.use(passport.session());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// configure a public directory to host static content
app.use(express.static(__dirname + '/public'));
app.use(favicon(__dirname + '/public/favicon.ico'));

require("./project/hubacademia.app.js")(app);

var port = process.env.PORT || 3000;
app.listen(port);
